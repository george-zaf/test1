import java.util.List;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {


        WebsiteOnList websiteOnList = new WebsiteOnList();
        // Create an Arraylist to store all the known websites
        List<String> popularWebsites = websiteOnList.mergeArrays();

        while (true) {

            // In every iteration we have to update the website list
            // Very important in order to stay updated without restarting the program...
            popularWebsites = websiteOnList.mergeArrays();

            // Get User/Proxy input
            Scanner scan = new Scanner(System.in);
            System.out.println("Provide manually a website to check: ");
            String checkWebsite = scan.next();

            // Check if the provided website is already in the 500 most visited websites list or if it has already been visited by the user
            boolean onPopularList = new WebsiteOnList().checkIfSiteOnList(popularWebsites, checkWebsite);
            // If the site is not in the popular list then check for typosquatting
            boolean newWebsite = true;
            if (!onPopularList){
                for (int i = 0; i < popularWebsites.size(); i++) {
                    if (TypoFinder.typosquatting(popularWebsites.get(i), checkWebsite) == true) {
                        System.out.println(popularWebsites.get(i) +  " was found . It could be a typosquatting attack.");
                        System.out.println("Do you want to proceed to this website? Types 'yes' or press any key ");
                        // If user proceeds then the site has to be added to the list
                        String userPreference = scan.next();
                        if(userPreference.equalsIgnoreCase("yes")){
                            FileOperations.writeUserVisitedWebsite(checkWebsite);
                        }
                        newWebsite = false;
                        // Add break to limit similar sites to ano if multiple exists.
                        // Only show the first one to the user
                        break;
                    }
                }
            }

            // Let's assume the code below is something like a controller.
            if(onPopularList){
                System.out.println("It's on the safe list. Safe to proceed");
            } else{
                if(newWebsite){
                    System.out.println("No typoSquatting found. It is probably a new site. Assume it is safe to proceed");
                    System.out.println("The website will be added on the user visited list");
                    FileOperations.writeUserVisitedWebsite(checkWebsite);
                }
            }
        }
    }


}




import java.util.ArrayList;
import java.util.List;

public class WebsiteOnList {

    // Merge the 2 arraylists to avoid code duplication later
    public List<String> mergeArrays(){

        // Retrieve the websides form both documents
        List<String> popularWebsites = new ArrayList<>(FileOperations.readPopularWebsites("top500Domains.csv"));
        List<String> userVisitedWebsites = new ArrayList<>(FileOperations.readUserVisitedWebsites("findUserDomains.txt"));

        popularWebsites.addAll(userVisitedWebsites);
        //System.out.println(popularWebsites);
        return popularWebsites;
    }


    public boolean checkIfSiteOnList(List<String> popularWebsites, String checkWebsite) {
        // Check  all visited websites
        boolean foundWebsite = false;
        for(int i=0; i< popularWebsites.size(); i++) {
            if (popularWebsites.get(i).equals(checkWebsite)) {
                return true;
            }
        }
        return false;
    }


}

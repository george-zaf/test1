import org.junit.Test;

import static org.junit.Assert.*;

public class TypoFinderTest {

    @Test
    public void missingOneCharacter() {
        TypoFinder typo = new TypoFinder();
        assertEquals(true, typo.missingOneCharacter("google", "gogle"));
        assertEquals(true, typo.missingOneCharacter("google", "googl"));
        assertEquals(true, typo.missingOneCharacter("google", "oogle"));
        assertEquals(true, typo.missingOneCharacter("google", "gogle"));
        assertEquals(false, typo.missingOneCharacter("google", "google"));
        assertEquals(false, typo.missingOneCharacter("google", "goegle"));
        assertEquals(false, typo.missingOneCharacter("google", "goog"));
        assertEquals(false, typo.missingOneCharacter("google", "goeglee"));
    }

    @Test
    public void extraOneCharacter() {
        TypoFinder typo = new TypoFinder();
        assertEquals(true, typo.extraOneCharacter("google", "gooogle"));
        assertEquals(true, typo.extraOneCharacter("google", "googlee"));
        assertEquals(true, typo.extraOneCharacter("google", "ggoogle"));
        assertEquals(true, typo.extraOneCharacter("google", "goeogle"));
        assertEquals(false, typo.extraOneCharacter("google", "google"));
        assertEquals(false, typo.extraOneCharacter("google", "goegle"));
        assertEquals(false, typo.extraOneCharacter("google", "goog"));
        assertEquals(false, typo.extraOneCharacter("google", "goog"));

    }

    @Test
    public void replacedCharacter() {
        TypoFinder typo = new TypoFinder();
        assertEquals(false, typo.replacedCharacter("google", "google"));
        assertEquals(false, typo.replacedCharacter("google", "gooogle"));
        assertEquals(false, typo.replacedCharacter("google", "gogle"));
        assertEquals(false, typo.replacedCharacter("google", "ooggle"));
        assertEquals(true, typo.replacedCharacter("google", "goggle"));
    }


    @Test
    public void adjacentSwappedCharachters() {
        TypoFinder typo = new TypoFinder();
        assertEquals(false, typo.adjacentSwappedCharachters("google", "eooglg"));
        assertEquals(false, typo.adjacentSwappedCharachters("google", "gooogle"));
        assertEquals(false, typo.adjacentSwappedCharachters("google", "gogle"));
        assertEquals(false, typo.adjacentSwappedCharachters("google", "google"));
        assertEquals(false, typo.adjacentSwappedCharachters("google", "ogggle"));
        assertEquals(false, typo.adjacentSwappedCharachters("google", "ogogel"));
        assertEquals(true, typo.adjacentSwappedCharachters("google", "googel"));
        assertEquals(true, typo.adjacentSwappedCharachters("google", "ogogle"));
    }


}